# Create the player directories, if they don't already exist
mkdir -p /var/data/ply/{a..z}


# Run the mud, tail the logs
(../bin/vme & sleep 5 ; tail -f vme.log) & (../bin/mplex -w -p 4280 & sleep 5 ; tail -f mplex.log)
