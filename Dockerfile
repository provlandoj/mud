FROM debian:buster-slim

RUN apt-get update
RUN apt-get install -y bison flex-old libboost-all-dev build-essential gcc

COPY ./vme /mud

WORKDIR /mud/src
RUN make all

WORKDIR /mud/etc
RUN make all

WORKDIR /mud/zone
RUN ../bin/vmc -m -I../include/ *.zon

# Create global data directory
RUN mkdir -p /var/data

# Simlink the player storage directories to the global data directory
RUN mv /mud/lib/ply /var/data
RUN ln -s /var/data/ply /mud/lib/ply

COPY ./run.sh /mud

WORKDIR /mud/log

CMD /mud/run.sh
